import biota

tile_2007 = biota.LoadTile('~/SMFM/ALOS_data',-18, 29, 2007, lee_filter=True, forest_threshold = 10, area_threshold = 1)

tile_2010 = biota.LoadTile('~/SMFM/ALOS_data',-18, 29, 2010, lee_filter=True, forest_threshold = 10, area_threshold = 1)

tile_2017 = biota.LoadTile('~/SMFM/ALOS_data',-18, 29, 2017, lee_filter=True, forest_threshold = 10, area_threshold = 1)

# 2007 - 2010
tile_change = biota.LoadChange(tile_2007, tile_2010, change_area_threshold = 1, change_magnitude_threshold = 5)

change_type = tile_change.getChangeType(output = True)

# 2007 - 2017
tile_change = biota.LoadChange(tile_2007, tile_2017, change_area_threshold = 1, change_magnitude_threshold = 5)

change_type = tile_change.getChangeType(output = True)

